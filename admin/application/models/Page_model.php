<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
        }
				
        public function login($data) {

			$condition = "user_login.user_name =" . "'" . $data['username'] . "' AND " . "user_login.user_password =" . "'" . $data['password'] . "'";
			$this->db->select('user_login.*');
//			$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		public function checkuser($user) {

			$condition = "user_name =" . "'" . $user . "'";
			$this->db->select('*');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}

		public function get_module_img($img_name)
		{

			$this->db->where('image', $img_name);
			$result = $this->db->get('blog_modules');
			return $result->result();
		}

			// Read data from database to show data in admin page
		public function read_user_information($username) {
			$condition = "user_name =" . "'" . $username . "'";
			$this->db->select('user_login.*');
//			$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return $query->result();
			} else {
				return false;
			}
		}
		
		public function get_leads()
        {
				$this->db->select('leads.*');
//				$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
                $query = $this->db->get('leads');
                return $query->result();
        }
        
		public function get_usuarios()
        {
				$this->db->select('user_login.*');
//				$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
                $query = $this->db->get('user_login');
                return $query->result();
        }
		public function get_usuarios_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('user_login');
                return $query->result();
        }		
        
        
		public function get_notas_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('blog');
                return $query->result();
        }

        public function get_multimedia_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('multimedia');
                return $query->result();
        }

        public function get_categoria_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('categorias');
                return $query->result();
        }


        public function get_videos_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('videos');
                return $query->result();
        }	
        
		public function get_blog()
        {
				$this->db->select('blog.*');
//				$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
                $query = $this->db->get('blog');
                return $query->result();
        }


        public function get_multimedia()
        {
				$this->db->select('multimedia.*');
//				$this->db->join('provincia', 'provincia.id = user_login.provinciaId', 'left');
                $query = $this->db->get('multimedia');
                return $query->result();
        }


        public function get_videos()
        {
				$this->db->select('videos.*');
                $query = $this->db->get('videos');
                return $query->result();
        }


        public function get_categorias()
        {
			$this->db->select('categorias.*');
            $query = $this->db->get('categorias');
            return $query->result();
        }

        public function get_cat_nota($id)
        {
            $this->db->select('categorias_notas.*');
            $this->db->where('id_nota', $this->uri->segment(3));
            $this->db->where('id_categoria', $id);
            $query = $this->db->get('categorias_notas');
            return $query->result();
        }

        public function get_modules_id($id)
        {
				$this->db->where('id_blog', $id);
                $query = $this->db->get('blog_modules');
                return $query->result();
        }

        public function get_modules_module_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('blog_modules');
                return $query->result();
        }
        
		
		/*INSERTS*/
		
		
		public function insert_usuario($data)
        {
            $this->db->insert('user_login', $data);
        }
        
		public function insert_notas($data)
        {
            $this->db->insert('blog', $data);
        }

        public function insert_multimedia($data)
        {
            $this->db->insert('multimedia', $data);
        }

        public function insert_categoria($data)
        {
            $this->db->insert('categorias', $data);
        }

        public function insert_video($data)
        {
            $this->db->insert('videos', $data);
        }

        public function insert_modules($data)
        {
            $this->db->insert('blog_modules', $data);
        }
        
		
		/*UPDATES*/
	
		public function update_notas($data)
        {
			$this->db->set('title', $data['title']);
			$this->db->set('width', $data['width']);
			$this->db->set('premium', $data['premium']);
			$this->db->set('portada', $data['portada']);
			$this->db->set('video', $data['video']);
			$this->db->set('modified_at', time());
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('blog');
        }

        public function update_categoria($data)
        {
			$this->db->set('nombre', $data['nombre']);
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('categorias');
        }

        public function insert_categoria_nota($data)
        {
            $this->db->insert('categorias_notas', $data);
        }

        public function update_multimedia($data)
        {
			$this->db->set('titulo', $data['titulo']);
			$this->db->set('descripcion', $data['descripcion']);
			$this->db->set('url', $data['url']);
			$this->db->set('image', $data['image']);
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('multimedia');
        }

        public function update_video($data)
        {
			$this->db->set('title', $data['title']);
			$this->db->set('width', $data['width']);
			$this->db->set('premium', $data['premium']);
			$this->db->set('video', $data['video']);
			$this->db->set('modified_at', time());
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('videos');
        }
        
		public function update_usuario($data)
        {
			$this->db->set('name', $data['name']);
			$this->db->set('provinciaId', $data['provinciaId']);
			$this->db->set('vendedor_id', $data['vendedor_id']);
			$this->db->set('administrator', $data['administrator']);
			$this->db->set('lastname', $data['lastname']);
			$this->db->set('user_email', $data['user_email']);
			if($data['user_password']<>''){
				$this->db->set('user_password', $data['user_password']);
			}
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('user_login');
        }

        public function get_productos_multimedia()
        {
        	$result = $this->db->get('multimedia');
        	return $result->result();
        }


        public function get_productos_multimedia_id($id)
        {	
        	$this->db->where('id', $id);
        	$result = $this->db->get('multimedia');
        	return $result->result();
        }

        public function update_order_modules($data)
        {
				$orden = explode(',',$data['orden']);
				$ids = explode(',',$data['ids']);
				$i=0;
				foreach ( $ids as $id ){
					$this->db->set('order', $orden[$i]);
					$this->db->where('id', $id);
					$this->db->update('blog_modules');
					$i++;
				}
        }

        public function update_order_gral($data)
        {
				$orden = explode(',',$data['orden']);
				$ids = explode(',',$data['ids']);
				$i=0;
				foreach ( $ids as $id ){
					$this->db->set('order', $orden[$i]);
					$this->db->where('id', $id);
					$this->db->update('blog');
					$i++;
				}
        }


        public function update_order_videos($data)
        {
				$orden = explode(',',$data['orden']);
				$ids = explode(',',$data['ids']);
				$i=0;
				foreach ( $ids as $id ){
					$this->db->set('order', $orden[$i]);
					$this->db->where('id', $id);
					$this->db->update('videos');
					$i++;
				}
        }

        public function update_modules($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('blog_modules', $data);
        }

        public function update_modules_tag($data, $id)
        {
			$this->db->where('id', $id);
            $this->db->update('blog_modules', $data);
        }
		
		/*REMOVES*/
		
		public function remove_usuario()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('user_login');
        }

        public function remove_modules()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('blog_modules');
        }
        
        
		public function remove_nota()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('blog');
        }

        public function remove_multimedia()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('multimedia');
        }

        public function remove_categoria()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('categorias');
        }

        public function remove_video()
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->delete('videos');
        }
        
}

?>