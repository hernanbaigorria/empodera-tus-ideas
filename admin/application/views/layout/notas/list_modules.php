<div class="listado">
	<div class="col-sm-12 home-tools">
	<div class="col-sm-8">
		<h2>MODULOS</h2>
	</div>
	<div class="col-sm-4">
		<a href="<?php echo base_url()?>notas/add_modules/new/?tempid=<?php echo $_GET['tempid'];?>"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
	</div>
	</div>
	<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th width="40">ID</th>
				<th>titulo</th>
				<th width="40">Editar</th>
				<th width="40">Eliminar</th>
			</tr>
		</thead>
		<tbody class="sortable">
			<?php
				$html='';
				foreach ( $info as $fila ){
					$html.='<tr class="list-sort" id="'.$fila->{'id'}.'">
						<td>'.$fila->{'order'}.'</td>
						<td>'.$fila->{'titulo'}.'</td>
						<td align="center"><a href="'.base_url().'notas/edit_modules/'.$fila->{'id'}.'/?tempid='.$_GET['tempid'].'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
						<td align="center"><a href="#" data-href="'.base_url().'notas/remove_modules/'.$fila->{'id'}.'/?tempid='.$_GET['tempid'].'" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
					</tr>';
				}
				echo $html;
			?>				
		</tbody>
	</table>
</div>

<br style="clear:both;"/>