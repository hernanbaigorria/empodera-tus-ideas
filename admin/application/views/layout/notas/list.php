<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
				<div class="row">
					<div class="col-xs-8 col-md-8">
						<h2>NOTAS DE BLOG</h2>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="<?php echo base_url()?>notas/add/"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
					</div>
				</div>
			</div>
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="40">Orden</th>
						<th>T&iacute;tulo</th>
						<th>Ancho</th>
						<th>Premium</th>
						<th>URL</th>
						<th width="90"></th>
						<th style="max-width:40px">Editar</th>
						<th style="max-width:40px">Eliminar</th>
					</tr>
				</thead>
				<tbody class="sortable-general">
					<?php
						$html='';
						foreach ( $info as $fila ){
							
							$premium = "<B>NO</B>";
							if($fila->premium == 1) $premium= "<B>CONTENIDO PREMIUM</B>";
						
						   if($fila->{"user_name"} != "admin"):
						
							$html.='<tr class="list-sort-gral" id="'.$fila->{'id'}.'">
								<td style="cursor: move;">'.$fila->{'order'}.'</td>
								<td style="cursor: move;">'.$fila->{'title'}.'</td>
								<td><b>'.$fila->{'width'}.'</b></td>
								<td>'.$premium.'</td>
								<td style="text-align:center;vertical-align: middle;color:#0d2ea0;"><a href='.base_url().'../blog/ver/'.$fila->id.' target="_blank" style="color:#0d2ea0;;font-size: 12px;"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
								';
								if($fila->publico == 1):
									$html.='<td style="text-align:center"><a href="'.base_url().'/notas/no_publicar/?id_nota='.$fila->id.'" style="font-size: 12px;background: #7d91d4;padding: 5px 10px;display: inline-block;color: #fff;text-align: center;">No publicar</a></td>';
								else:
									$html.='<td style="text-align:center"><a href="'.base_url().'/notas/publicar/?id_nota='.$fila->id.'" style="font-size: 12px;background: #0d2ea0;padding: 5px 10px;display: inline-block;color: #fff;text-align: center;">Publicar</a></td>';
								endif;
								$html.='<td align="center"><a href="'.base_url().'notas/edit/'.$fila->{'id'}.'/"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
								<td align="center"><a href="#" data-href="'.base_url().'notas/remove/'.$fila->{'id'}.'/" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
							</tr>';
							
							endif;
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>