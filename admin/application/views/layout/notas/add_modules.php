<div class="home-content" style="margin-top:0px; padding-top:20px;">
	<div class="navbar-inner">
		<ul class="nav nav-tabs">
		  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">NUEVO MODULO</a></li>
		</ul>
	</div>
	<div class="tab-content" id="adm_form">
	  <div class="tab-pane active" id="tab1">
		 <form method="post" action="<?php echo base_url()?>notas/save_modules/?tempid=<?php echo $_GET['tempid'];?>">
	 	  	<div class="td-input">
				<b>Titulo de modulo:</b><br>
				<input type="text" name="titulo" id="titulo">
			</div>

	 	  	<div class="td-input">
	 	  		<b>Texto:</b><br>
	 			<div id="wysihtml5-toolbar">
					<header>
						<ul class="commands">
						  <li class="command" title="Make text bold (CTRL + B)" data-wysihtml5-command="bold" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Make text italic (CTRL + I)" data-wysihtml5-command="italic" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Insert an unordered list" data-wysihtml5-command="insertUnorderedList" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Insert an ordered list" data-wysihtml5-command="insertOrderedList" href="javascript:;" unselectable="on"></li>	
						  <li class="command" title="Insert a link" data-wysihtml5-command="createLink" href="javascript:;" unselectable="on"></li>
						  <!--<li class="command" title="Insert an image" data-wysihtml5-command="insertImage" href="javascript:;" unselectable="on" ></li>-->
						  <li class="command" title="Insert speech" data-wysihtml5-command="insertSpeech" href="javascript:;" unselectable="on" style="display: none;"></li>
						  <li class="action" title="Show HTML" data-wysihtml5-action="change_view" href="javascript:;" unselectable="on"></li>
						</ul>
					  </header>
					  <div data-wysihtml5-dialog="createLink" style="display: none;">
						<label>
							Link:
							<input data-wysihtml5-dialog-field="href" value="http://">
						</label>
						<a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
					  </div>
					  <div data-wysihtml5-dialog="insertImage" style="display:none;">
						  <label>
							Image:
							<input data-wysihtml5-dialog-field="src" id="inpt_wysihtml5" value="">
							<div class="pull-right">
								<div id="galeriawysihtml5" ></div>
								<div id="main_uploader" >
								<div class="uploader-idwysihtml5">
								<div id="uploaderwysihtml5" align="left">
								<input id="uploadifywysihtml5" type="file" class="uploader" />
								</div>
								</div>
								<div id="filesUploaded" style="display:none;"></div>
								
								<div id="thumbHeightwysihtml5" style="display:none;" >960</div>
								<div id="thumbWidthwysihtml5" style="display:none;" >1400</div>
								</div>
							</div>
							
						  </label>
						  <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
					</div>
				</div>
	 	  	</div>
			
			<textarea id="wysihtml5-textarea" name="texto"></textarea>
			<div class="td-input">
				<b>Imagen: (Opcional)</b><br>
				<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" readonly>
				<div id="main_uploader">
					<div class="uploader-id1">
						<div id="uploader1" align="left">
							<input id="uploadify1" type="file" class="uploader" />
						</div>
					</div>
					<div id="filesUploaded"></div>
					<div id="thumbHeight1" style="display:none;">800</div>
					<div id="thumbWidth1" style="display:none;">1131</div>
				</div>
				<div id="galeria1" class="upload-galeria"></div>
			</div>
			
		 </form>
	  </div>
	</div>
	<div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	<a href="<?php echo base_url()?>notas/list_modules/?tempid=<?php echo $_GET['tempid'];?>"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
   
</div>
<br style="clear:both;"/>