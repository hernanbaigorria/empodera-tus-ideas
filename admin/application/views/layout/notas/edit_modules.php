<div class="home-content" style="margin-top:0px; padding-top:20px;">
	<div class="navbar-inner">
		<ul class="nav nav-tabs">
		  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">EDITAR MODULO</a></li>
		</ul>
	</div>
	<div id="adm_form">
		<form method="post" action="<?php echo base_url()?>notas/update_modules/<?php echo $info[0]->{'id'};?>/?tempid=<?php echo $_GET['tempid'];?>">
			<div class="tab-content">
			  <div class="tab-pane active" id="tab1">
			  	<div class="td-input">
			  		<b>Texto:</b><br>
					<input type="text" name="titulo" value="<?php echo $info[0]->{'titulo'}?>">
			  	</div>
			  	<div class="td-input">
	 	  		<b>Texto:</b><br>
	 			<div id="wysihtml5-toolbar">
					<header>
						<ul class="commands">
						  <li class="command" title="Make text bold (CTRL + B)" data-wysihtml5-command="bold" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Make text italic (CTRL + I)" data-wysihtml5-command="italic" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Insert an unordered list" data-wysihtml5-command="insertUnorderedList" href="javascript:;" unselectable="on"></li>
						  <li class="command" title="Insert an ordered list" data-wysihtml5-command="insertOrderedList" href="javascript:;" unselectable="on"></li>	
						  <li class="command" title="Insert a link" data-wysihtml5-command="createLink" href="javascript:;" unselectable="on"></li>
						  <!--<li class="command" title="Insert an image" data-wysihtml5-command="insertImage" href="javascript:;" unselectable="on" ></li>-->
						  <li class="command" title="Insert speech" data-wysihtml5-command="insertSpeech" href="javascript:;" unselectable="on" style="display: none;"></li>
						  <li class="action" title="Show HTML" data-wysihtml5-action="change_view" href="javascript:;" unselectable="on"></li>
						</ul>
					  </header>
					  <div data-wysihtml5-dialog="createLink" style="display: none;">
						<label>
							Link:
							<input data-wysihtml5-dialog-field="href" value="http://">
						</label>
						<a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
					  </div>
					  <div data-wysihtml5-dialog="insertImage" style="display:none;">
						  <label>
							Image:
							<input data-wysihtml5-dialog-field="src" id="inpt_wysihtml5" value="">
							<div class="pull-right">
								<div id="galeriawysihtml5" ></div>
								<div id="main_uploader" >
								<div class="uploader-idwysihtml5">
								<div id="uploaderwysihtml5" align="left">
								<input id="uploadifywysihtml5" type="file" class="uploader" />
								</div>
								</div>
								<div id="filesUploaded" style="display:none;"></div>
								
								<div id="thumbHeightwysihtml5" style="display:none;" >960</div>
								<div id="thumbWidthwysihtml5" style="display:none;" >1400</div>
								</div>
							</div>
							
						  </label>
						  <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
					</div>
				</div>
	 	  	</div>
			
			<textarea id="wysihtml5-textarea" name="texto"><?php echo $info[0]->texto ?></textarea>			 
				<div class="td-input">
					<b>Imagen:</b><br>
					<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->{'image'}?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id1">
							<div id="uploader1" align="left">
								<input id="uploadify1" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight1" style="display:none;" >800</div>
						<div id="thumbWidth1" style="display:none;" >1440</div>
					</div>
					<div id="galeria1" class="upload-galeria">
						<?php if($info[0]->{'image'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'image'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'image'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>

				<div class="td-input">
					<b>¿Imagen con etiquetas?:</b><br>
					<select name="etiqueta">
						<option value="">Seleccionar...</option>
						<option value="1" <?php if($info[0]->tag == 1): echo "selected"; endif; ?>>Si</option>
						<option value="0" <?php if($info[0]->tag == 0): echo "selected"; endif; ?>>No</option>
					</select>
				</div>

		  	</div>
		   </div>
	   </form>
   </div>
   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
   <a href="<?php echo base_url()?>notas/list_modules/?tempid=<?php echo $_GET['tempid'];?>"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
</div>

<br style="clear:both;"/>