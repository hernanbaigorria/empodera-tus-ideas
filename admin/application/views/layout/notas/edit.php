<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>

<style type="text/css">
	.dropdown.bootstrap-select {width:100% !important;}
	.btn.dropdown-toggle {background: none;width: 100%;padding: 5px 10px;border-radius: 0;box-shadow: none;font-size: 12px;color: #000;}
</style>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <li role="presentation"><a href="#tab2" data-toggle="tab">Modulos</a></li>
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>notas/update/<?php echo $info[0]->{'id'}?>/">
				<?php if(isset($error)){ ?>
				<div class="td-input error-usuario">
				<?php }else{ ?>
				<div class="td-input">
				<?php } ?>
					<b>T&iacute;tulo:</b><br>
					<input type="text" name="title" id="title" value="<?php if(isset($info[0]->{'title'})){echo $info[0]->{'title'};}?>">
				</div>

				<div class="td-input">
					<b>Categorias</b><br>
					<select name="categorias[]" class="selectpicker my-select" multiple data-live-search="true" title="Seleccionar Categoria">
						<?php foreach($categorias as $categoria): ?>
							<?php $categoria_nota = $this->page_model->get_cat_nota($categoria->id); ?>
							<option value="<?=$categoria->id?>" <?php if(isset($categoria_nota) && $categoria_nota[0]->id_categoria == $categoria->id): echo "selected"; endif; ?>><?=$categoria->nombre?></option>
						<?php endforeach; ?>
					</select>
				</div>
				
				<div class="td-input">
					<b>Ancho:</b><br>
					<select name="width" id="width">
						<option <?php if($info[0]->{'width'} === "col-md-4"){echo " selected ";}?> value="col-md-4">30%</option>						
						<option <?php if($info[0]->{'width'} === "col-md-8"){echo " selected ";}?> value="col-md-8">60%</option>						
						<option <?php if($info[0]->{'width'} === "col-md-12"){echo " selected ";}?> value="col-md-12">100%</option>						
					</select>
				</div>
				
				
				<div class="td-input">
					<b>Premium:</b><br>
					<select name="premium" id="premium">
						<option <?php if($info[0]->{'premium'} == 0){echo " selected ";}?> value="0">NORMAL</option>
						<option <?php if($info[0]->{'premium'} == 1){echo " selected ";}?> value="1">PREMIUM</option>
					</select>
				</div>				
				<div class="td-input">
					<b>Imagen:</b><br>
					<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->{'portada'}?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id1">
							<div id="uploader1" align="left">
								<input id="uploadify1" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight1" style="display:none;" >800</div>
						<div id="thumbWidth1" style="display:none;" >1440</div>
					</div>
					<div id="galeria1" class="upload-galeria">
						<?php if($info[0]->{'portada'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'portada'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'portada'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>
				<div class="td-input">
					<b>Video (Opcional):</b><br>
					<input type="text" name="galeria2_input" id="galeria2_input" class="img-input" value="<?php echo $info[0]->{'video'}?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id2">
							<div id="uploader2" align="left">
								<input id="uploadify2" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight2" style="display:none;" >800</div>
						<div id="thumbWidth2" style="display:none;" >1440</div>
					</div>
					<div id="galeria2" class="upload-galeria">
						<?php if($info[0]->{'video'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'video'}?>',function(){}); $('#galeria2_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'video'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>
			 
			 </form>

			 <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
			 <a href="<?php echo base_url()?>notas/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>

		  </div>
		  <div class="tab-pane" id="tab2">
			 <iframe src="<?php echo base_url()?>notas/list_modules/new/?tempid=<?php echo $info[0]->{'id'};?>" onload="iframeLoaded(this)"></iframe>
		  </div>
	   </div>
	   
	</div>
</div>
<br style="clear:both;"/>
