<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notas extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');


	    
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		$info =  $this->page_model->get_blog();
		$data['info']=$info;
		
		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/notas/list', $data, TRUE); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$data['categorias'] = $this->page_model->get_categorias();
		$this->template->write_view('content', 'layout/notas/add', $data, TRUE); 
		$this->template->render();
	}
	
	public function save()
	{
		if (isset($this->session->userdata['logged_in'])) 
		{		

				$data = array(
					'title' => $_POST['title'],
					'width' => $_POST['width'],
					'premium' => $_POST['premium'],
					'portada' => $_POST['galeria1_input'],
					'video' => $_POST['galeria2_input'],
				);
				$this->db->insert('blog', $data);
				$id_blog = $this->db->insert_id();

				foreach($_POST['categorias'] as $key => $categoria):

					$data_categoria = array(
						'id_categoria' => $categoria,
						'id_nota' => $id_blog
					);

					$this->page_model->insert_categoria_nota($data_categoria);
				endforeach;
				redirect('notas/');
		}
		else{
			redirect('login/');
		}
		
	}

	public function no_publicar()
	{
		$data = array('publico' => 0);
		$this->db->where('id', $_GET['id_nota']);
		$this->db->update('blog', $data);
		redirect('notas/');
	}

	public function publicar()
	{
		$data = array('publico' => 1);
		$this->db->where('id', $_GET['id_nota']);
		$this->db->update('blog', $data);
		redirect('notas/');
	}
	public function update()
	{
		if (isset($this->session->userdata['logged_in']))
		{

			$data = array(
				'title' => $_POST['title'],
				'width' => $_POST['width'],
				'premium' => $_POST['premium'],
				'portada' => $_POST['galeria1_input'],
				'video' => $_POST['galeria2_input'],
			);

			foreach($_POST['categorias'] as $key => $categoria):

				$data_categoria = array(
					'id_categoria' => $categoria,
					'id_nota' => $this->uri->segment(3)
				);

				$this->page_model->insert_categoria_nota($data_categoria);
			endforeach;
			
			$this->page_model->update_notas($data);
			redirect('notas/');
		}else{
			redirect('login/');
		}
		
	}
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_nota();
			redirect('notas/');
		}else{
			redirect('login/');
		}
		
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_notas_id($this->uri->segment(3));		
		$data['info']=$info;
		$data['categorias'] = $this->page_model->get_categorias();
		$this->template->write_view('content', 'layout/notas/edit', $data, TRUE); 
		$this->template->render();
	}
	
	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('home/');
	}
	
	public function SendForm()
	{
		$parmsJSON = (isset($_POST['_p']))?$_POST['_p']:$_GET['_p'];
		$parmsJSON = urldecode(base64_decode ( $parmsJSON ));
		$JSON = new Services_JSON();
		$parmsJSON = $JSON->decode($parmsJSON);
		$asunto = $parmsJSON->{'asunto'};
		$mensaje = rawURLdecode($parmsJSON->{'mensaje'});
		$name = $parmsJSON->{'name'};
		$email = $parmsJSON->{'email'};
		$para = $parmsJSON->{'para'};
			
		$mAIL = new MAIL;
		$mAIL->From($email,$name);
        							
		$mAIL->AddTo($para);
		 $mAIL->Subject(utf8_encode($asunto));
									
	     $contact['message_body'] = $mensaje;
							        
		$mAIL->Html($contact['message_body']);
									
	 
		$cON = $mAIL->Connect("smtp.gmail.com", (int)465, "diego.mantovani@gmail.com", "p4t0f30p4t0f30", "tls") or die(print_r($mAIL->Result));
        $mAIL->Send($cON) ? $sent = true : $sent = false;
		$mAIL->Disconnect();
		
		
		if(!$sent) {
		 print '{"resultado":"NO","error":"'.$mAIL->Result.'"}';
		} else {
		  print '{"resultado":"OK"}';
		}

		exit;
			
	}


	/*Tab Modules*/
	public function list_modules(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/products.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');

		$CI =& get_instance();
		
		$info =  $this->page_model->get_modules_id($_GET['tempid']);	
		$data['info']=$info;
		
		$this->template->write_view('content', 'layout/notas/list_modules', $data, TRUE); 
		$this->template->render();
	}
	public function add_modules(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/products.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/modules.js?v='.time().'');
		
		$CI =& get_instance();
		$data['countries']='';
		
		$this->template->write_view('content', 'layout/notas/add_modules', $data, TRUE); 
		$this->template->render();
	}
	public function edit_modules(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/products.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/modules.js?v='.time().'');
		
		$CI =& get_instance();
		
		$info =  $this->page_model->get_modules_module_id($this->uri->segment(3));	
		$data['info']=$info;
		
		$this->template->write_view('content', 'layout/notas/edit_modules', $data, TRUE); 
		$this->template->render();
	}
	public function save_modules(){
		if (isset($this->session->userdata['logged_in'])) {
			
			$data = array(
				'id_blog' => $this->input->get('tempid'),
				'titulo' => $_POST['titulo'],
				'texto' => $_POST['texto'],
				'image' => $_POST['galeria1_input'],
			);
			
			$this->page_model->insert_modules($data);
			redirect('notas/list_modules/?tempid='.$this->input->get('tempid'));
		}else{
			redirect('login/');
		}
		
	}
	public function update_modules(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'id_blog' => $this->input->get('tempid'),
				'titulo' => $_POST['titulo'],
				'texto' => $_POST['texto'],
				'tag' => $_POST['etiqueta'],
				'image' => $_POST['galeria1_input'],
			);
			$this->page_model->update_modules($data);
			if($_POST['etiqueta'] == 1){
				redirect('tag/index.html?id='.$this->uri->segment(3).'');
			} else {
				redirect('notas/list_modules/?tempid='.$this->input->get('tempid'));
			}
		}else{
			redirect('login/');
		}
		
	}
	public function remove_modules(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_modules();
			redirect('notas/list_modules/?tempid='.$this->input->get('tempid'));
		}else{
			redirect('login/');
		}
		
	}

	public function update_order_modules(){
		$data = array(
			'orden' => $_POST['orden'],
			'ids' => $_POST['ids']
		);
		$this->page_model->update_order_modules($data);
	}

	public function update_order_gral(){
		$data = array(
			'orden' => $_POST['orden'],
			'ids' => $_POST['ids']
		);
		$this->page_model->update_order_gral($data);
	}

	


	public function uploads()
	{
	      $data = array();
	      if (!empty($_FILES['file']['name'])) {
	          $filesCount = count($_FILES['file']['name']);
	          for ($i = 0; $i < $filesCount; $i++) {
	              $_FILES['uploadFile']['name'] = str_replace(",","_",$_FILES['file']['name'][$i]);
	              $_FILES['uploadFile']['type'] = $_FILES['file']['type'][$i];
	              $_FILES['uploadFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
	              $_FILES['uploadFile']['error'] = $_FILES['file']['error'][$i];
	              //$_FILES['uploadFile']['size'] = $_FILES['file']['size'][$i];
	              //Directory where files will be uploaded
	              $uploadPath = './uploads/';
	              $config['upload_path'] = $uploadPath;
	              // Specifying the file formats that are supported.
	              $config['max_size'] = 0;
	              $config['max_width'] = 0;
	              $config['max_height'] = 0;
	              $config['overwrite'] = TRUE;
	              $config['allowed_types'] = '*';
	              //$config['overwrite'] = FALSE;
	              //$config['remove_spaces'] = TRUE;
	              $this->load->library('upload', $config);
	              $this->upload->initialize($config);
	              if ($this->upload->do_upload('uploadFile')) {
	                  $fileData = $this->upload->data();
	                  $uploadData[$i]['file_name'] = $fileData['file_name'];
	              } else {
	              	$error = array('error' => $this->upload->display_errors());
	              	            
      	            //$this->load->view('error', $error);
      	            print_r($error);
	              }
	          }
	          if (!empty($uploadData)) {
	              $list=array();
	              foreach ($uploadData as $value) {
	                  array_push($list, $value['file_name']);
	              }
	        echo json_encode($list);
	      	}
	    }
	}
}
