<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	   
	   
	} 
	 
	
	public function index()
	{

		// Busco la imagen para obtener el id
		$modulo = $this->page_model->get_module_img(basename($_POST['img']));

		// Reemplazo el id que viene de la app para usar mi id obtenido
		$codigo = $_POST['texto'];
		$nuevo = str_replace("#myimagelinks", '#tag'.$modulo[0]->id, $codigo);

		// Actualizo el modulo con el id y el codigo actualizado
		$data = array('codigo' => $nuevo);
		$this->page_model->update_modules_tag($data, $modulo[0]->id);

		// Respondo con el id asi puedo redirreccionar al back
		echo $modulo[0]->id_blog;
		exit;
	}

	public function productos()
	{
		$productos = $this->page_model->get_productos_multimedia();

		$contenido.='<select id="productosshow" style="padding: 5px 25px;margin-bottom: 15px;margin-top: 5px;font-size: 14px;"><option value="">Seleccionar...</option>';
		foreach($productos as $producto){

			$contenido.='<option value='.$producto->{'id'}.'>'.$producto->{'titulo'}.'</option>';

		}
		$contenido.='</select>';
		echo $contenido;
	}

	public function getproductos()
	{
		$producto = $this->page_model->get_productos_multimedia_id($_POST['productId']);

		$html='<p><img src="'.base_url().'../../asset/img/uploads/'.$producto[0]->image.'"><strong>'.$producto[0]->titulo.'</strong>'.$producto[0]->descripcion.'</p><a href="'.$producto[0]->url.'" target="_blank">Ver más</a>';
		echo $html;
	}
}
