$(document).ready(function(){
	 $('#list').DataTable({
		 "bSort": true,
		 "stateSave": true
	 });
	
	 
	/*CAMPOS A VALIDAR*/
	jsonDatos=eval('({"campos":['+
		'{"campo":"titulo","validacion":"B"}'+
		']})');
	
	/*UPLOAD CONFIG*/
	var w=99999;
	var h=99999;
	var path='../../../../asset/img/uploads/';
	
	
	var maxWidth=99999;
	var thWidth=99999;
	var thHeight=99999;		
	var tipo='unica';
	var allowedTypes='jpg,png,gif,mp4'
	var callback=function(){console.log('upload complete');}


	//uploaderwysihtml5('wysihtml5',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);

	uploaderNoCrop('1',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
	uploaderNoCrop('2',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);

	$('.sortable').sortable({	
		update: function(event, ui) { 
			var order=1;
			var i=0;
			var ids='';
			var ordenes='';
			$('.sortable').find('.list-sort').each(function(){
				ids+=(ids==''?'':',')+$(this).attr('id');
				ordenes+=(ordenes==''?'':',')+order;
				order++;			
			});	
			//alert(ordenes+' | '+ids);
			updOrder(ordenes, ids);
		},
		items: ".list-sort"
	 });


	$('.sortable-general').sortable({	
		update: function(event, ui) { 
			var order=1;
			var i=0;
			var ids='';
			var ordenes='';
			$('.sortable-general').find('.list-sort-gral').each(function(){
				ids+=(ids==''?'':',')+$(this).attr('id');
				ordenes+=(ordenes==''?'':',')+order;
				order++;			
			});	
			//alert(ordenes+' | '+ids);
			updOrderGral(ordenes, ids);
		},
		items: ".list-sort-gral"
	 });

	$('.sortable-videos').sortable({	
		update: function(event, ui) { 
			var order=1;
			var i=0;
			var ids='';
			var ordenes='';
			$('.sortable-videos').find('.list-sort-videos').each(function(){
				ids+=(ids==''?'':',')+$(this).attr('id');
				ordenes+=(ordenes==''?'':',')+order;
				order++;			
			});	
			//alert(ordenes+' | '+ids);
			updOrderVideos(ordenes, ids);
		},
		items: ".list-sort-videos"
	 });

});



function updOrder(ordenes, ids){
	
	url=base_url+'notas/update_order_modules/';
		$.ajax({
			url: url,
			data:{ 'orden' : ordenes, 'ids' : ids  },
			success: function(response){
				location.reload();
			}
		});
}


function updOrderGral(ordenes, ids){
	
	url=base_url+'notas/update_order_gral/';
		$.ajax({
			url: url,
			data:{ 'orden' : ordenes, 'ids' : ids  },
			success: function(response){
				location.reload();
			}
		});
}


function updOrderVideos(ordenes, ids){
	
	url=base_url+'videos/update_order_videos/';
		$.ajax({
			url: url,
			data:{ 'orden' : ordenes, 'ids' : ids  },
			success: function(response){
				location.reload();
			}
		});
}