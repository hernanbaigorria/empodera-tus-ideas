
<section class="header-blog" style="background:#EA7200;">
	<div class="container d-flex align-items-center">
		<img src="<?php echo base_url() ?>asset/img/rayo_blanco.png" class="img-fluid">
		<h3>Videos</h3>
	</div>
</section>
<section class="grilla-blogs module-videos" style="padding-top:0px;">
	<!-- ENTRADA DESTACADA FULL -->
		<?php foreach($video as $b):?>
		    <?php if($b->width == 'col-md-12'): ?>
				<div class="col-12 mascara-video fullconten-ipad video d-flex p-0 flex-column video<?=$b->id?>" data-toggle="modal" data-target="#video<?=$b->id?>" style="position:relative;min-height:800px;">
                        <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
					<div class="info-video d-flex align-items-center justify-content-center flex-column">
						<img src="<?php echo base_url() ?>asset/img/play_icon.png" class="img-fluid">
					</div>
				</div>
				<div class="col-12 p-0 content-title-videos" style="margin-bottom:25px;">
				    <div class="row m-0 w-100">
				        <div class="col-12 p-0">
				            <p style="min-height:inherit;font-size:20px;"><?=$b->title?></p>
				        </div>
				    </div>
				</div>
		    <?php endif; ?>
		<?php endforeach; ?>
	<!-- ENTRADA DESTACADA FULL -->

	<!-- ENTRADA RESTO DE GRILLA -->
	<div class="container">
		<div class="row m-0 w-100">
			<?php foreach($video as $b):?>
			    <?php if($b->width != 'col-md-12'): ?>
				<div class="col-12 col-md-6 col-lg-4  text-center video<?=$b->id?> " data-toggle="modal" data-target="#video<?=$b->id?>">
                    <div class="col-12 mascara-video min-ipad-pro d-flex flex-column p-0 video" style="position:relative;min-height:300px;">
                        <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
						<div class="info-video d-flex align-items-center justify-content-center flex-column">
							<img src="<?php echo base_url() ?>asset/img/play_icon.png" class="img-fluid">
						</div>
                    </div>
                    <div class="col-12 p-0 content-title-videos" style="margin-bottom:25px;">
                        <div class="row m-0 w-100">
                            <div class="col-12 p-0">
                                <p style="min-height:inherit;"><?=$b->title?></p>
                            </div>
                        </div>
                    </div>
				</div>
				<?php endif; ?>
	        <?php endforeach; ?>
		</div>
	</div>

	<!-- ENTRADA RESTO DE GRILLA -->
</section>

<?php foreach($video as $vid):?>
  <!-- The Modal -->
  <div class="modal modal-videos no-premium-modal" id="video<?=$vid->id?>">
    <div class="modal-dialog">
      <div class="modal-content" >
        <!-- Modal body -->
        <div class="modal-body" style="padding:70px 40px;">            
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <video class="video-modal-enter vid-play-<?=$vid->id?>" controls style="width:100%;max-width:100%;">
                <source src="<?php echo base_url() ?>asset/img/uploads/<?=$vid->video?>" type="video/mp4">
            </video>
            <h5><?=$vid->title?></h5>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <?php foreach($video as $vid):?>
      <script type="text/javascript">
          $(".video<?=$vid->id?>").click(function() {
            $('.vid-play-<?=$vid->id?>').trigger('play');
          });
      </script>
  <?php endforeach; ?>