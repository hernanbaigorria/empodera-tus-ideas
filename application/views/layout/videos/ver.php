<?php if(!empty($nota[0]->video)): ?>
<section class="portada-entry-blog d-flex" style="position:relative;">
	<video src="<?php echo base_url() ?>asset/img/uploads/<?=$nota[0]->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
<?php else: ?>
	<section class="portada-entry-blog d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?php echo $nota[0]->portada ?>');">
<?php endif; ?>
	<div class="content-title" style="z-index:9;">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-1"></div>
				<div class="col-12 col-md-6">
					<h3><?php echo $nota[0]->title ?></h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="body-entry">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-10">
				<div style="color: #000;margin: 35px 0;font-family: 'catamaranthin';font-size: 17px;">
				<?php foreach($nota_module as $module): ?>
					<?php echo $module->texto ?>
					<?php if(!empty($module->image)): ?>
						<img src="<?php echo base_url() ?>asset/img/uploads/<?php echo $module->image ?>" class="img-fluid"  style="margin:20px 0">
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<section class="pie-footer-entry">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-10">
				<div class="row w-100 m-0">
					<div class="col-12 p-0 d-flex align-items-center justify-content-center flex-wrap">
						<img src="<?php echo base_url() ?>asset/img/rayo_gris_blog_ver.png" class="img-fluid">

						<!-- Go to www.addthis.com/dashboard to customize your tools -->
						<div class="addthis_inline_share_toolbox_b335 redes-compartir"></div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>