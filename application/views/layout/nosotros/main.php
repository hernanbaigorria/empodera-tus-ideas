<section class="header-nosotros d-flex align-items-center" style="background: url('<?=base_url()?>asset/img/back_nosotros.png');">
	<div class="container hidden-xs">
		<div class="row">
			<div class="col-12 col-xl-5 col-md-4 col-sm-4"></div>
			<div class="col-12 col-xl-7 col-md-8 col-sm-8">
				<h4>Nuestra visión es<br>potenciar la tuya.</h4>
				<p>El programa Empodera Tus Ideas es una iniciativa de Brother para acompañarte en el desarrollo de tu emprendimiento. ¿Necesitas transformarte digitalmente? ¿Quisieras lograr mayor eficiencia en tus procesos? ¿Qué tecnología es la adecuada para tu proyecto? Aquí encontrarás información de valor para avanzar.</p>
			</div>
		</div>
	</div>
</section>
<?php if(true): ?>
<section class="header-nosotros visible-xs" style="min-height:inherit;margin-bottom:40px;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-xl-5 col-md-4 col-sm-4"></div>
			<div class="col-12 col-xl-7 col-md-8 col-sm-8">
				<h4>Nuestra visión es<br>potenciar la tuya.</h4>
				<p>El programa Empodera Tus Ideas es una iniciativa de Brother para acompañarte en el desarrollo de tu emprendimiento. ¿Necesitas transformarte digitalmente? ¿Quisieras lograr mayor eficiencia en tus procesos? ¿Qué tecnología es la adecuada para tu proyecto? Aquí encontrarás información de valor para avanzar.</p>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>


<section class="module-01-nosotros">
	<div class="row w-100 m-0 align-items-center">
		<div class="col-12 col-md-1 col-lg-2"></div>
		<div class="col-12 col-md-5 col-lg-4">
			<p>Una plataforma con contenido exclusivo, notas de actualidad, recomendación de los expertos y material de marketing listo para personalizar, para que puedas iniciar o potenciar tu emprendimiento hoy.</p>
		</div>
		<div class="col-12 col-md-6 col-lg-6 p-0" style="margin-top:auto;">
			<img src="<?php echo base_url() ?>asset/img/right_persona_nosotros.png" class="img-fluid">
		</div>
	</div>
</section>

<section class="module-02-nosotros">
	<div class="col-12 p-0">
		<img src="<?php echo base_url() ?>asset/img/img_module_nosotros01.jpg" class="img-fluid">
		<div class="text-mod">
			<p>Un espacio de actualización permanente para PyMEs, que te permite estar conectado con todo lo que necesitas para maximizar tus recursos y tu tiempo, para tomar
			decisiones acertadas y fortalecer
			tus proyectos. Inspírate, aprende,
			optimiza. Estamos a tu lado.</p>
		</div>
	</div>
</section>

<section class="module-03-nosotros">
	<div class="col-12 text-center">
		<img src="<?php echo base_url() ?>asset/img/logo+rayo_nosotros.png" class="img-fluid">
	</div>
</section>