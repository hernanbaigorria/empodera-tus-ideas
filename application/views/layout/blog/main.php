<section class="header-blog">
	<div class="container d-flex align-items-center">
		<img src="<?php echo base_url() ?>asset/img/rayo_blanco.png" class="img-fluid">
		<h3>Blog</h3>
	</div>
</section>
<section class="categorias">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-2">
				<h5>Categorías:</h5>
			</div>
			<div class="col-12 col-md-10">
				<?php foreach($categorias as $categoria): 
					$sintildescat = $this->page_model->eliminar_tildes($categoria->nombre);
					$slugcat = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildescat)));
					?>

					<a href="<?=base_url()?>blog/?categoria=<?=$slugcat?>" <?php if($slugcat == $_GET['categoria']):?> class="active-cat" <?php endif; ?> ><?=$categoria->nombre?></a>
				<?php endforeach; ?>
				<a href="<?=base_url()?>blog/" <?php if(!isset($_GET['categoria'])):?> class="active-cat" <?php endif; ?>>Todas</a>
			</div>
		</div>
	</div>
</section>
<section class="grilla-blogs">
	<!-- ENTRADA DESTACADA FULL -->
	
		<?php 
		foreach($blog as $b):
		
		$sintildes = $this->page_model->eliminar_tildes($b->title);
		$slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildes)));
	    $href= "";  
	    $extra = "";
	    if (!isset($this->session->userdata['logged_in_front']) && $b->premium == 1)
	    {
	        $extra = ' data-toggle="modal" data-target="#noPremium" ';
	        $href="#";
	    }
	    else
	    {
	        $href=base_url()."blog/ver/".$b->id."/".$slug."";
	    }

	    if(count($blog) <= 1) $b->width="col-md-11";
		else if(count($blog) == 2) $b->width="col-md-6";
		else if(count($blog) == 3) $b->width="col-md-4";
	    
	    ?>
	    <?php if($b->premium == 1 & $b->width == 'col-md-12'): ?>
	    	<?php if(!empty($b->video)): ?>
            <a href="<?=$href?>" <?php if($href == '#'): ?>id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/".$slug.""?>" data-toggle="modal" data-target="#noPremium" <?php endif; ?> class="col-12 p-0 destacado-full d-flex video">
                <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
                
            <?php else: ?>
            <a href="<?=$href?>" <?php if($href == '#'): ?>id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/"?>" data-toggle="modal" data-target="#noPremium" <?php endif; ?> class="col-12 p-0 destacado-full d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?=$b->portada?>');">
            <?php endif; ?>
				<div class="row w-100 m-0">
					<div class="col-12 p-0 col-md-7 content-entrada-home">
						<p><?=$this->page_model->limit_words($b->title,99);?></p>
                        <div class="icon-premium d-flex align-items-center">
                            <h6>Solo Para<br>Suscriptores Premium</h6>
                            <img src="<?php echo base_url() ?>asset/img/rayo_verde.png" class="img-fluid">
                        </div>
					</div>
				</div>
			</a>
	    <?php endif; ?>
		<?php endforeach; ?>
	<!-- ENTRADA DESTACADA FULL -->

	<!-- ENTRADA RESTO DE GRILLA -->
	<div class="container">
		<div class="row m-0 w-100">
			<?php foreach($blog as $b):
				$sintildes = $this->page_model->eliminar_tildes($b->title);
				$slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildes)));
			    $href= "";	
			    $extra = "";
			    if (!isset($this->session->userdata['logged_in_front']) && $b->premium == 1)
			    {
			    	$extra = ' data-toggle="modal" data-target="#noPremium" ';
			    	$href="#";
			    }
			    else
			    {
			    	$href=base_url()."blog/ver/".$b->id."/".$slug."";
			    }
				
				if($b->width == 'col-md-12' && $b->premium==0) $b->width="col-md-12";
			    
			    
			    
			    ?>
			    <?php // if($b->width != 'col-md-12'): ?>
				<?php if($b->premium != 1): ?>
				<a href="<?=$href?>" class="col-12 <?=$b->width?>" id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/".$slug.""?>" <?=$extra?>>
					<?php if(!empty($b->video)): ?>
	                <div class="col-12 rest-grilla p-0 d-flex video">
	                    <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
	                    
	                <?php else: ?>
	                <div class="col-12 rest-grilla p-0 d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?=$b->portada?>');">
	                <?php endif; ?>
	                    <div class="col-12 p-0 content-entrada-home">
	                        <div class="row m-0 w-100">
	                            <div class="col-12 col-md-12 p-0">
	                                <p><?=$this->page_model->limit_words($b->title,99);?></p>
	                            </div>
	                        </div>
	                        <?php if($b->premium == 1): ?>
	                        <div class="icon-premium d-flex align-items-center">
	                            <h6>Solo Para<br>Suscriptores Premium</h6>
	                            <img src="<?php echo base_url() ?>asset/img/rayo_verde.png" class="img-fluid">
	                        </div>
	                        <?php endif; ?>
	                    </div>
	                </div>
				</a>
				<?php endif; ?>
	        <?php endforeach; ?>
		</div>
	</div>

	<!-- ENTRADA RESTO DE GRILLA -->
</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<?php foreach($blog as $b):?>
    <script type="text/javascript">
        $("#modalPremiumUrl<?=$b->id?>").click(function(){

            $('.url-input').val($(this).attr('data-url'));
            //alert($(this).attr('data-url'));

        });
    </script>
<?php endforeach; ?>