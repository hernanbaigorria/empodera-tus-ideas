<?php if(!empty($nota[0]->video)): ?>
<section class="portada-entry-blog video-interna d-flex" style="position:relative;">
	<video src="<?php echo base_url() ?>asset/img/uploads/<?=$nota[0]->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
<?php else: ?>
	<section class="portada-entry-blog d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?php echo $nota[0]->portada ?>');">
<?php endif; ?>
	<div class="content-title" style="z-index:9;">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-1"></div>
				<div class="col-12 col-md-6">
					<h3><?php echo $nota[0]->title ?></h3>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="body-entry">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-10">
				<div style="color: #000;margin: 35px 0;font-family: 'catamaranthin';font-size: 17px;">
				<?php foreach($nota_module as $module): ?>
					<?php echo $module->texto ?>
					<?php if(!empty($module->image)): ?>
						<?php if($module->tag == 1): ?>
						<img id="tag<?=$module->id?>" src="<?php echo base_url() ?>asset/img/uploads/<?php echo $module->image ?>" class="img-fluid"  style="margin:20px 0">
						<?php else: ?>
							<img src="<?php echo base_url() ?>asset/img/uploads/<?php echo $module->image ?>" class="img-fluid"  style="margin:20px 0">
						<?php endif; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				</div>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<section class="pie-footer-entry">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-10">
				<div class="row w-100 m-0">
					<div class="col-12 p-0 d-flex align-items-center justify-content-center flex-wrap">
						<img src="<?php echo base_url() ?>asset/img/rayo_gris_blog_ver.png" class="img-fluid">
						<?php $sintildes = $this->page_model->eliminar_tildes($nota[0]->title);$slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildes))); ?>
						<!-- Go to www.addthis.com/dashboard to customize your tools -->
						<div class="addthis_inline_share_toolbox_b335 redes-compartir" data-url="<?php echo base_url() ?>blog/ver/<?php echo $nota[0]->id ?>/<?php echo $slug ?>" data-title="<?php echo $nota[0]->title ?>"></div>
						
					</div>
					<script type="text/javascript"> 
					var addthis_share = { 
					   url : "<?php echo base_url() ?>blog/ver/<?php echo $nota[0]->id ?>/<?php echo $slug ?>" , 
					   title : "<?php echo $nota[0]->title ?>"
					} </script> 
				</div>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<script src="<?php echo base_url() ?>asset/tag/assets/js/lib/jquery.min.js"></script>
<script src="<?php echo base_url() ?>asset/tag/assets/lib/jquery.imagelinks.min.js"></script>
<script type="text/javascript">
<?php foreach($nota_module as $module): ?>
	<?php if(!empty($module->image)): ?>
		<?php if($module->tag == 1): ?>
			<?php echo $module->codigo ?>
		<?php endif; ?>
	<?php endif; ?>
<?php endforeach; ?>
</script>