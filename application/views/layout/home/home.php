<section classs="slider-top">
    <div class="owl-carousel owl-theme" id="homeTop">
        <div class="item">
            <img src="<?php echo base_url() ?>asset/img/desk_borther_empoder_new.png" class="img-fluid hidden-xs">
            <img src="<?php echo base_url() ?>asset/img/responsive_borther_empoder_new.png" class="img-fluid visible-xs">
        </div>
    </div>
</section>
<div class="modulo-02">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4">
                <div class="paralelogramo d-flex align-items-center" style="text-align:right;background:#fff">
                    <h3 style="margin-left: auto;margin-bottom: 50px;">Tus proyectos<br>son nuestra<br>inspiración</h3>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3 p-0">
                <div class="paralelogramo d-flex align-items-center">
                    <p>Estamos contigo,
                        acercándote soluciones
                        innovadoras para que
                        puedas maximizar recursos
                        y fortalecer tu PyME, tu
                        emprendimiento. Para que sigas proyectando sin límites, reinventando tu negocio,
                        creciendo con seguridad hacia tus objetivos. ¿Tienes una idea? Ahora tienes un aliado para desarrollarla.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 d-flex align-items-center">
                <img src="<?php echo base_url() ?>asset/img/persona.png" class="img-fluid">
            </div>
            <div class="col-12 col-md-12 col-lg-1"></div>
        </div>
    </div>
</div>
<section class="grilla-blogs">
    <!-- ENTRADA DESTACADA FULL -->
    <?php foreach($blog as $b):
        $sintildes = $this->page_model->eliminar_tildes($b->title);
        $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildes)));
        $href= "";  
        $extra = "";
        if (!isset($this->session->userdata['logged_in_front']) && $b->premium == 1)
        {
            $extra = ' data-toggle="modal" data-target="#noPremium" ';
            $href="#";
        }
        else
        {
            $href=base_url()."blog/ver/".$b->id."/".$slug."";
        }
        
        ?>
        <?php if($b->premium == 1 & $b->width == 'col-md-12'): ?>
            <?php if(!empty($b->video)): ?>
            <a href="<?=$href?>" <?php if($href == '#'): ?>data-toggle="modal" id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/".$slug.""?>" data-target="#noPremium" <?php endif; ?> class="col-12 p-0 destacado-full d-flex video">
                <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
                
            <?php else: ?>
            <a href="<?=$href?>" <?php if($href == '#'): ?>data-toggle="modal" id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/"?>" data-target="#noPremium" <?php endif; ?> class="col-12 p-0 destacado-full d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?=$b->portada?>');">
            <?php endif; ?>
                <div class="row w-100 m-0">
                    <div class="col-12 p-0 col-xl-7 content-entrada-home">
                        <p><?=$this->page_model->limit_words($b->title,99);?></p>
                        <div class="icon-premium d-flex align-items-center">
                            <h6>Solo Para<br>Suscriptores Premium</h6>
                            <img src="<?php echo base_url() ?>asset/img/rayo_verde.png" class="img-fluid">
                        </div>
                    </div>
                </div>
            </a>
        <?php endif; ?>
    <?php endforeach; ?>
    <!-- ENTRADA DESTACADA FULL -->
    <!-- ENTRADA RESTO DE GRILLA -->
    <div class="row m-0 w-100">
        <?php foreach($blog as $b):

            $sintildes = $this->page_model->eliminar_tildes($b->title);
            $slug = preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($sintildes)));

            $href= "";	
            $extra = "";
            if (!isset($this->session->userdata['logged_in_front']) && $b->premium == 1)
            {
            	$extra = ' data-toggle="modal" data-target="#noPremium" ';
            	$href="#";
            }
            else
            {
            	$href=base_url()."blog/ver/".$b->id."/".$slug."";
            }
            
            
            
            ?>
            <?php if($b->width != 'col-md-12'): ?>
            <a href="<?=$href?>" id="modalPremiumUrl<?=$b->id?>" data-url="<?=base_url()."blog/ver/".$b->id."/".$slug.""?>" class="col-12 <?=$b->width?>" <?=$extra?>>
                <?php if(!empty($b->video)): ?>
                <div class="col-12 rest-grilla p-0 d-flex video">
                    <video src="<?php echo base_url() ?>asset/img/uploads/<?=$b->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
                    
                <?php else: ?>
                <div class="col-12 rest-grilla p-0 d-flex" style="background:url('<?php echo base_url() ?>asset/img/uploads/<?=$b->portada?>');">
                <?php endif; ?>
                    <div class="col-12 p-0 content-entrada-home">
                        <div class="row m-0 w-100">
                            <div class="col-12 col-xl-9 p-0">
                                <p><?=$this->page_model->limit_words($b->title,99);?></p>
                            </div>
                        </div>
                        <?php if($b->premium == 1): ?>
                        <div class="icon-premium d-flex align-items-center">
                            <h6>Solo Para<br>Suscriptores Premium</h6>
                            <img src="<?php echo base_url() ?>asset/img/rayo_verde.png" class="img-fluid">
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </a>
            <?php endif; ?>
        <?php endforeach; ?>
        <a href="<?php echo base_url() ?>blog/" class="col-12 col-md-4">
            <div class="col-12  ver-mas-blogs d-flex align-items-center justify-content-center flex-column">
                <img src="<?php echo base_url() ?>asset/img/rayo_blanco.png" class="img-fluid">
                <h4>EXPLORAR TODAS LAS NOTAS</h4>
            </div>
        </a>
    </div>
    <!-- ENTRADA RESTO DE GRILLA -->
</section>

<section class="module-videos" id="videos">
	<div class="col-12 p-0" style="position:relative;">
		<div class="row m-0">
			<div class="col-12 col-md-5 col-lg-3">
				<div class="paralelogramo-title-video d-flex align-items-center">
					<h3>Videos</h3>
				</div>
			</div>
			<div class="col-12 col-md-2 col-lg-1"></div>
			<div class="col-12 col-md-5 col-lg-5 mg-t-p">
				<p>Recorre las claves de un emprendimiento exitoso junto a expertos del medio. Mantente actualizado junto a Brother.</p>
			</div>
		</div>

		<div class="row mg-top-row">
			<div class="col-12 col-md-1 col-xl-2"></div>
			<div class="col-12 col-md-10 col-xl-8">
				<div  class="row w-100 m-0">
                    <?php foreach($videos as $video):?>
                        <?php if($video->width == 'col-md-12'): ?>
    					<div class="col-12" >
                            <div class="col-12 mascara-video fullconten-ipad d-flex flex-wrap p-0 video video<?=$video->id?>" data-toggle="modal" data-target="#video<?=$video->id?>" style="position:relative;min-height:500px;">
                                <video src="<?php echo base_url() ?>asset/img/uploads/<?=$video->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
        						<div class="info-video d-flex align-items-center justify-content-center flex-column">
        							<img src="<?php echo base_url() ?>asset/img/play_icon.png" class="img-fluid">
        						</div>
                            </div>
                            <div class="col-12 p-0 content-title-videos" style="margin-bottom:25px;">
                                <div class="row m-0 w-100">
                                    <div class="col-12 p-0">
                                        <p style="min-height:inherit;font-size:20px;"><?=$video->title?></p>
                                    </div>
                                </div>
                            </div>
    					</div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php foreach($videos as $video):?>
                        <?php if($video->width != 'col-md-12'): ?>
    					<div class="col-12 col-md-6 col-lg-4  text-center video<?=$video->id?>" data-toggle="modal" data-target="#video<?=$video->id?>">
                            <div class="col-12 mascara-video min-ipad-pro d-flex flex-wrap p-0 video" style="position:relative;min-height:300px;">
                                <video src="<?php echo base_url() ?>asset/img/uploads/<?=$video->video?>" autoplay loop playsinline muted style="opacity:1;"></video>
        						<div class="info-video d-flex align-items-center justify-content-center flex-column">
        							<img src="<?php echo base_url() ?>asset/img/play_icon.png" class="img-fluid">
        						</div>
                            </div>
                            <div class="col-12 p-0 content-title-videos" >
                                <div class="row m-0 w-100">
                                    <div class="col-12 p-0">
                                        <p style="min-height:inherit;"><?=$video->title?></p>
                                    </div>
                                </div>
                            </div>
    					</div>
                        <?php endif; ?>
                    <?php endforeach; ?>
					<div class="col-12 col-md-12 col-lg-4">
						<a href="<?php echo base_url() ?>videos/" class="d-flex align-items-center justify-content-center flex-column rayo-videos">
							<img src="<?php echo base_url() ?>asset/img/rayo_blanco.png" class="img-fluid">
							<p>EXPLORAR TODOS LOS VIDEOS</p>
						</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-1 col-xl-2"></div>
		</div>
		
	</div>
</section>

<section class="module-herramientas" id="herramientas">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>Herramientas</h3>
			</div>
			<div class="col-12 col-md-6">
				<p>Encuentra un amplio abanico de material profesional de mercadeo editable y gratuito para agregar valor y diferenciar tu negocio.</p>
			</div>
			<div class="col-12 col-md-6"></div>
			<div class="col-12 col-md-3">
				<img src="<?php echo base_url() ?>asset/img/bloques_empoder-08.png" class="img-fluid" style="border-radius:7px;width:100%;">
				<a href="https://www.creativecenter.brother/es-es/business/industry/finance-legal" target="_blank">Servicios financieros y legales</a>
			</div>
			<div class="col-12 col-md-3">
				<img src="<?php echo base_url() ?>asset/img/bloques_empoder-09.png" class="img-fluid" style="border-radius:7px;width:100%;">
				<a href="https://www.creativecenter.brother/es-es/business/industry/food-drink" target="_blank">Restaurantes</a>
			</div>
			<div class="col-12 col-md-3">
				<img src="<?php echo base_url() ?>asset/img/bloques_empoder-10.png" class="img-fluid" style="border-radius:7px;width:100%;">
				<a href="https://www.creativecenter.brother/es-es/business/industry/technology" target="_blank">Tecnología</a>
			</div>
			<div class="col-12 col-md-3">
				<img src="<?php echo base_url() ?>asset/img/bloques_empoder-11.png" class="img-fluid" style="border-radius:7px;width:100%;">
				<a href="https://www.creativecenter.brother/es-es/business/industry/education" target="_blank">Educación</a>
			</div>
		</div>
	</div>
</section>
<?php foreach($videos as $video):?>
  <!-- The Modal -->
  <div class="modal modal-videos no-premium-modal" id="video<?=$video->id?>">
    <div class="modal-dialog">
      <div class="modal-content" >
        <!-- Modal body -->
        <div class="modal-body" style="padding:70px 40px;">            
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <video class="video-modal-enter vid-play-<?=$video->id?>" controls style="width:100%;max-width:100%;">
                <source src="<?php echo base_url() ?>asset/img/uploads/<?=$video->video?>" type="video/mp4">
            </video>
            <h5><?=$video->title?></h5>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<?php foreach($blog as $b):?>
    <script type="text/javascript">
        $("#modalPremiumUrl<?=$b->id?>").click(function(){

            $('.url-input').val($(this).attr('data-url'));
            //alert($(this).attr('data-url'));

        });
    </script>
<?php endforeach; ?>

<?php foreach($videos as $video):?>
    <script type="text/javascript">
        $(".video<?=$video->id?>").click(function() {
          $('.vid-play-<?=$video->id?>').trigger('play');
        });
    </script>
<?php endforeach; ?>
<?php if(false): ?>
  <?php if (isset($this->session->userdata['logged_in_front'])) { ?>
    <div class="modal-suscripto">
        <div class="content-modal">
            <button type="button" class="close close-modal-suscriptor" data-dismiss="modal">×</button>
            <h1>Ya eres un miembro Premium!</h1>
            <img src="<?php echo base_url() ?>asset/img/rayo_azul.png" class="img-fluid">
        </div>
    </div>
  <?php } ?>
<?php endif; ?>