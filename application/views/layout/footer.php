<?php if (!isset($this->session->userdata['logged_in_front'])) { ?>
<section class="newsletter" id="newsletter">
	<div class="col-12 p-0" style="position:relative;">
		<div class="row m-0">
			<div class="col-12 col-md-4 col-lg-3">
				<div class="paralelogramo d-flex align-items-center" style="background:#1965DF">
					<h3>Suscríbete gratis<br>y disfruta de<br>contenido<br>premium</h3>
				</div>
			</div>
		</div>
		<div class="row row-absolute-news align-items-center">
			<div class="col-12 col-md-4 col-lg-4"></div>
			<div class="col-12 col-md-5 col-lg-4 text-center">
				
				<form class="sendform" method="POST" action="<?=base_url()?>home/send_form">
					<input type="text" name="pais" placeholder="País" required>
					<input type="hidden" name="url" class="url-input" value="https://www.empoderatusideas.com?subscription=t">
					<input type="text" name="nombre" placeholder="Nombre" required style="background:#E4E4E5;">
					<input type="email" name="email" placeholder="Email" required >
					<div class="info-suscr">
			        	<h6>Seleccione la información que le gustaría recibir de Brother:</h6>
			        	<div class="d-flex align-items-center">
				        	<input type="checkbox" id="infoproduct" name="informacion_producto">
				        	<label for="infoproduct">Información del producto (incluidas actualizaciones de software y servicio, y ofertas especiales)</label>
			        	</div>
			        	<div class="d-flex align-items-center">
				        	<input type="checkbox" id="noticias" name="informacion_producto">
				        	<label for="noticias">Noticias (incluidos consejos, proyectos y eventos especiales)</label>
			        	</div>
		        	</div>
					<input type="submit" value="Suscribirme">
				</form>
				<a href="#" data-toggle="modal" data-target="#Login" style="margin-top: 15px;display:block;">
					Ingresar
				</a>
				
				<img src="<?php echo base_url() ?>asset/img/rayo_azul.png" class="img-fluid">
			</div>
			<div class="col-12 col-md-3 col-lg-4"></div>

		</div>
	</div>
</section>
<?php } ?>
<section class="mapa-sitio">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-12 d-flex align-items-center justify-content-center flex-wrap">
				<h3><a href="<?php echo base_url() ?>blog/">Notas</a></h3>
				<h3><a href="<?php echo base_url() ?>#videos">Videos</a></h3>
				<h3><a href="<?php echo base_url() ?>#herramientas">Herramientas</a></h3>
				<h3><a href="<?php echo base_url() ?>nosotros/">Acerca de Nosotros </a></h3>
				<?php if (!isset($this->session->userdata['logged_in_front'])) { ?>				
					<h3><a href="#newsletter">Suscripción</a></h3>
				<?php } ?>
				<a href="https://www.facebook.com/brotheramericalatina" class="redes" target="_blank">
					<i class="fab fa-facebook"></i>
				</a>
			</div>
			<div class="col-12 text-center">
				<p><a href="<?php echo base_url() ?>politicas_de_privacidad/">Politicas de Privacidad</a> / <a href="<?php echo base_url() ?>terminos_y_condiciones/">Terminos y Condiciones</a></p>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8">
				<p>Latinoamérica | © 1995-2020, Brother International Corporation. Todos los derechos reservados.</p>
			</div>
			<div class="col-12 col-md-4 text-right">
				<a href="https://global.brother/en/gateway" target="_blank">Brother Global</a>
			</div>
		</div>
	</div>
</footer>