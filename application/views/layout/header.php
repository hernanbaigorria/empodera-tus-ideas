<header class="col-12">
	<div class="container">
		<div class="row w-100 m-0 align-items-center">
			<div class="col-12 col-md-2 text-center">
				<a href="<?php echo base_url() ?>">
					<img src="<?php echo base_url() ?>asset/img/brother_logo.png" class="img-fluid">
				</a>
			</div>
			<div class="col-4 col-md-5">
				
			</div>
			<div class="col-12 col-md-5 text-right">
				
				<?php if (isset($this->session->userdata['logged_in_front'])) { ?>
					<p class="user-name">Conectado como <b><?=$this->session->userdata['logged_in_front']["name"]?></b> [<a style="color:#fff" href="<?=base_url()?>home/logout">Salir</a>]</p>
				<?php } else { ?>

					<p class="user-name"><a style="color:#fff" href="#" data-toggle="modal" data-target="#Login">¿Eres premium? <b>Ingresá aquí</b></a></p>			
					<p class="user-name"><a style="color:#fff" href="#" data-toggle="modal" data-target="#noPremium">Subscribirme a <b>premium</b></a></p>			
				<?php } ?>
				

				<div class="icon-sear" style="display:none;">
					<i class="fas fa-search"></i>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="search-content" style="display:none;">
	<div class="container">
		<div class="row m-0">
			<div class="col-12 col-md-3 p-0"></div>
			<div class="col-12 col-md-6 p-0">
				<form method="get" action="#">
					<div class="row w-100 align-items-center m-0">
						<div class="col-12 col-md-9">
							<input type="text" name="s" placeholder="Buscar entrada">
						</div>
						<div class="col-12 col-md-3">
							<input type="submit" value="Buscar">
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 col-md-3 p-0"></div>
		</div>
	</div>
</div>
<div class="content-menu">
	<div class="container">
		<div class="row">
			<ul>
				<li><a href="<?php echo base_url() ?>blog/">Blog</a></li>
				<li><a href="<?php echo base_url() ?>videos/">Videos</a></li>
				<li><a href="<?php echo base_url() ?>#herramientas">Herramientas</a></li>
				<li><a href="<?php echo base_url() ?>nosotros/">Acerca de nosotros</a></li>
				
				<?php if (!isset($this->session->userdata['logged_in_front'])) { ?>				
					<li><a href="#newsletter">Suscripción</a></li>
				<?php } ?>
				
			</ul>
		</div>
	</div>
</div>
<?php if (isset($this->session->userdata['logged_in_front'])) { ?>
<section class="suscripto-header">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center d-flex align-items-center justify-content-center">
				<h3>Ya eres un miembro Premium!</h3>
				<img src="<?php echo base_url() ?>asset/img/rayo_azul.png" class="img-fluid">
			</div>
		</div>
	</div>
</section>
<?php } ?>