<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="<?= $description ?>">
	  <meta name="keywords" content="<?= $keywords ?>">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="<?= $title ?>" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="<?= base_url().$image ?>" />
	  <meta property="og:description" content="<?= $description ?>" />
	  


	  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="https://global.brother/-/media/global/common/img/icon/brother_icon.ashx">
	  <link rel="apple-touch-icon" size="152x152" href="https://global.brother/-/media/global/common/img/icon/apple-touch-icon.ashx">

	  <!-- you can use your own animation library and css3 classes -->
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/tag/assets/lib/effect.css"> 
	  <!-- imagelinks styles -->
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/tag/assets/lib/imagelinks.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/tag/assets/lib/imagelinks.theme.default.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/tag/assets/lib/imagelinks.theme.dark.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/tag/assets/lib/imagelinks.theme.dots.css">

	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.png"> 
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.min.css'>

	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">


	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/main.css?v=<?=time()?>" type="text/css" media="all" />
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/animations.css" media="all" />

	  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,800,900" rel="stylesheet"> 
	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/font-awesome.min.css">
	  <script>var base_url = "<?php echo base_url() ?>";</script>

	  <!-- Global site tag (gtag.js) - Google Analytics -->
	  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175654919-1"></script>
	  <script>
	    window.dataLayer = window.dataLayer || [];
	    function gtag(){dataLayer.push(arguments);}
	    gtag('js', new Date());

	    gtag('config', 'UA-175654919-1');
	  </script>

	  <!-- Facebook Pixel Code -->
	  <script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '300991924501036');
	  fbq('track', 'PageView');
	  </script>
	  <noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=300991924501036&ev=PageView&noscript=1"
	  /></noscript>
	  <!-- End Facebook Pixel Code -->

	  <!-- Global site tag (gtag.js) - Google Ads: 636874387 -->
	  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-636874387"></script>
	  <script>
	    window.dataLayer = window.dataLayer || [];
	    function gtag(){dataLayer.push(arguments);}
	    gtag('js', new Date());

	    gtag('config', 'AW-636874387');
	  </script>

	  <!-- The following line must be placed in the head tag -->
	  <script type="text/javascript" src="https://p.teads.tv/teads-fellow.js" async="true"></script>
	  <script>
	    window.teads_e = window.teads_e || [];
	    window.teads_buyer_pixel_id = 1569;
	  </script>
	  
	  <?= $_styles ?>
	  
	  
	  <?php if(isset($_GET["subscription"]) && $_GET["subscription"] =="t"): ?>
		<!-- Event snippet for Subscribe Empodera Tus Ideas conversion page -->
		<script>
		  gtag('event', 'conversion', {'send_to': 'AW-636874387/dhWCCO2Y4PYBEJPd168C'});
		</script>
	  <?php endif; ?>
	  
	  
	  

   </head>
   <body>
	  <div class="main-content col-md-12 p-0">
	  <?= $header ?>        
	  <?= $content ?>
   	  <?= $footer ?>
	  </div>

	  
	  

	 <?php if (!isset($this->session->userdata['logged_in_front'])) { ?>				
	  <div class="float-button" data-toggle="modal" data-target="#noPremium">
	  	<div class="paralelogramo open-suscribirte d-flex align-items-center">
	  		<h3><strong>Suscríbete gratis</strong> y disfruta<br>de contenido premium</h3>
	  		<div class="open">
	  			<i class="fas fa-chevron-down"></i>
	  		</div>
	  	</div>
	  </div>
	  <?php } ?>

	  <!-- The Modal -->
	  <div class="modal no-premium-modal" id="noPremium">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <!-- Modal body -->
	        <div class="modal-body">
		        
				<?php if (isset($this->session->userdata['logged_in_front'])) { ?>
					<h1>Ya eres un miembro Premium!</h1>
				<?php } else { ?>
				
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<img src="<?php echo base_url() ?>asset/img/title_premium.png" class="img-fluid">
	          <p>Para disfrutar de este contenido premium<br>¡suscríbete gratis!</p>
	          <form class="sendform" method="POST" action="<?=base_url()?>home/send_form">
	          	<input type="hidden" name="url" class="url-input">
	          	<input type="text" name="pais" placeholder="País" required>
	          	<input type="text" name="nombre" placeholder="Nombre" required style="background:#E4E4E5;">
	          	<input type="email" name="email" placeholder="Email" required>
	          	<div class="info-suscr">
	  	        	<h6>Seleccione la información que le gustaría recibir de Brother:</h6>
	  	        	<div class="d-flex align-items-center">
	  		        	<input type="checkbox" id="infoproduct" name="informacion_producto" required>
	  		        	<label for="infoproduct">Información del producto (incluidas actualizaciones de software y servicio, y ofertas especiales)</label>
	  	        	</div>
	  	        	<div class="d-flex align-items-center">
	  		        	<input type="checkbox" id="noticias" name="informacion_producto" required>
	  		        	<label for="noticias">Noticias (incluidos consejos, proyectos y eventos especiales)</label>
	  	        	</div>
	          	</div>
	          	<input type="submit" value="Suscribirme">
	          	<div class="error-suscrib" style="display:none;margin: 15px 0;color: #000;">Usted ya esta suscripto haga <a href="#" data-toggle="modal" data-target="#Login">click aqui</a> para ingresar</div>
	          </form>
	          <a href="#" data-toggle="modal" data-target="#Login" style="margin-top: 15px;display: inline-block;">
	          	Ingresar
	          </a>
	          <?php } ?>
	        </div>
	      </div>
	    </div>
	  </div>

	    <!-- The Modal -->
	    <div class="modal no-premium-modal" id="Login">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <!-- Modal body -->
	          <div class="modal-body">
	  			
	          	<button type="button" class="close" data-dismiss="modal">&times;</button>
	          	<img src="<?php echo base_url() ?>asset/img/title_premium.png" class="img-fluid">
	            <p>Si ya esta registrado ingrese su correo:</p>
	            <form class="sendLogin" method="POST" action="<?=base_url()?>home/login_form">
	            	<input type="hidden" name="url" class="url-input">
	            	<input type="email" name="email" placeholder="Email" required>
	            	<input type="submit" value="Ingresar">
	            	<div class="error-login" style="display:none;margin: 15px 0;color: #000;">Usted no esta registrado.</div>
	            </form>
	          </div>
	        </div>
	      </div>
	    </div>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

	  <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>

	  <!-- Go to www.addthis.com/dashboard to customize your tools -->
	  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59949fa17c898713"></script>

	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script> 
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	 <!-- Latest compiled and minified JavaScript -->
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

	 <!-- (Optional) Latest compiled and minified JavaScript translation files -->
	  
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
	  <?= $_scripts ?>
	  <?php 
	  $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
	  $url = $base_url . $_SERVER["REQUEST_URI"]."?subscription=t";
	   ?>
	  <script type="text/javascript">

	  	$(".float-button").click(function() {
	  	  $('.url-input').val('<?=$url?>');
	  	});

	  	$(".close").click(function() {
	  	  $('.video-modal-enter').trigger('pause');
	  	});

	  	$(".close-modal-suscriptor").click(function() {
	  	  $('.modal-suscripto').hide();
	  	});

	  	var figure = $(".video").hover( hoverVideo, hideVideo );

	  	function hoverVideo(e) {  
	  	    $('video', this).get(0).play(); 
	  	    $('video', this).addClass('opacity-1'); 
	  	}

	  	function hideVideo(e) {
	  	    //$('video', this).get(0).load();
	  	    $('video', this).removeClass('opacity-1'); 
	  	}
	  </script>


	  <script type="text/javascript">
	  	$(".sendform").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar-btn').addClass('disable');
	  		        $('.gif-load').show('slow');
	  		    },
	  		    success: function(response){
	  		    	if(response == 'error'){
	  		    		$('.error-suscrib').show();
	  		    	} else {
	  		    		gtag('event', 'suscripto', { 'event_action': 'enviar'});
	  		        	window.location.replace(response);
	  		    	}
	  		    }
	  		});
	  	});

	  	$(".sendLogin").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar-btn').addClass('disable');
	  		        $('.gif-load').show('slow');
	  		    },
	  		    success: function(response){
	  		    	if(response == 'error'){
	  		    		$('.error-login').show();
	  		    	} else {
	  		        	window.location.replace(response);
	  		    	}
	  		    }
	  		});
	  	});

	  </script>
	  <script type="text/javascript">
	  	$('.my-select').selectpicker();
	  </script>
	  <script type="text/javascript">
	  	$( ".icon-sear" ).click(function() {
	  	  $( ".search-content" ).slideToggle( "slow" );
	  	});
	  </script>
	  <script type="text/javascript">
	  	$('#homeTop').owlCarousel({
	  	  loop: true,
	  	  margin: 10,
	  	  nav: true,
	  	  navText: [
	  	    "<i class='fa fa-caret-left'></i>",
	  	    "<i class='fa fa-caret-right'></i>"
	  	  ],
	  	  autoplay: true,
	  	  autoplayHoverPause: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1
	  	    },
	  	    600: {

	  	      items: 1
	  	    },
	  	    1000: {
	  	      items: 1
	  	    }
	  	  }
	  	});
	  </script>

   </body>
</html>