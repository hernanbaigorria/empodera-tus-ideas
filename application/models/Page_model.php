<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
        }
        
        public $title;
        public $content;
        public $date;
        


        function eliminar_tildes($cadena){

            //Codificamos la cadena en formato utf8 en caso de que nos de errores
            //$cadena = utf8_encode($cadena);

            //Ahora reemplazamos las letras
            $cadena = str_replace(
                array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
                array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
                $cadena
            );

            $cadena = str_replace(
                array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
                array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
                $cadena );

            $cadena = str_replace(
                array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
                array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
                $cadena );

            $cadena = str_replace(
                array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
                array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
                $cadena );

            $cadena = str_replace(
                array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
                array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
                $cadena );

            $cadena = str_replace(
                array('ñ', 'Ñ', 'ç', 'Ç'),
                array('n', 'N', 'c', 'C'),
                $cadena
            );

            return $cadena;
        }


        public function limit_words($string, $word_limit) {
            $string = strip_tags($string);
            $words = explode(' ', strip_tags($string));
            $return = trim(implode(' ', array_slice($words, 0, $word_limit)));
            if(strlen($return) < strlen($string)){
            //$return .= '...';
            }
            return $return;
        }
        
        public function get_lead_id($id)
        {
            $this->db->where('email', $id);
            $result = $this->db->get('leads');
            return $result->result();
        }
        
		public function get_blog_home()
        {   

			$this->db->select('blog.*');
            $this->db->order_by('order', 'asc');
            $this->db->where('publico', 1);
            $this->db->limit(6);
            $query = $this->db->get('blog');
            return $query->result();
        }

        public function get_video_home()
        {   

            $this->db->select('videos.*');
            $this->db->order_by('order', 'asc');
            $this->db->limit(3);
            $query = $this->db->get('videos');
            return $query->result();
        }

        public function get_blog_all()
        {   

            $this->db->select('blog.*');
            if(isset($_GET['categoria'])):
                $this->db->select('categorias.nombre as categoria');
                $this->db->join('categorias_notas','categorias_notas.id_nota = blog.id');
                $this->db->join('categorias','categorias.id = categorias_notas.id_categoria');
                $categoria = str_replace('-',' ', $_GET['categoria']);
                $this->db->like('categorias.nombre', $categoria);
            endif;
            $this->db->where('publico', 1);
            $this->db->order_by('order', 'asc');
			
			$this->db->group_by("blog.id");
			
            $query = $this->db->get('blog');
            return $query->result();
        }

        public function get_video_all()
        {   

            $this->db->select('videos.*');
            $this->db->order_by('order', 'asc');
            $query = $this->db->get('videos');
            return $query->result();
        }

        public function get_nota_id($id)
        {
            $this->db->select('blog.*');
            $this->db->where('id', $id);
            $query = $this->db->get('blog');
            return $query->result();
        }


        public function get_video_id($id)
        {
            $this->db->select('videos.*');
            $this->db->where('id', $id);
            $query = $this->db->get('videos');
            return $query->result();
        }

        public function get_nota_module_id($id)
        {
            $this->db->select('blog_modules.*');
            $this->db->where('id_blog', $id);
            $this->db->join('blog', 'blog.id = blog_modules.id_blog');
            $query = $this->db->get('blog_modules');
            return $query->result();
        }
		

        public function get_categorias()
        {
            $this->db->select('*');
            $this->db->order_by('nombre', 'asc');
            $result = $this->db->get('categorias');
            return $result->result();
        }
		
}

?>