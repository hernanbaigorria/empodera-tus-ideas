<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		if(isset($_GET['id_user'])):
			$get_user = $this->page_model->get_lead_id($_GET['id_user']);

			$session_data = array(
				'country' => $get_user[0]->country,
				'name' => $get_user[0]->name,
	           	'email' => $get_user[0]->email,
			);

			$this->session->set_userdata('logged_in_front', $session_data);
			redirect('/');
		endif;
		/*
		$this->load->library('GetResponse'); 
		$this->getresponse->enterprise_domain = 'cloud.oxford.com.ar';
		
		$result = $this->getresponse->getContacts();
		$data['contactos']= $result;
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		*/
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    	
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
		
		// getting blog
		$data["blog"] = $this->page_model->get_blog_home();
		$data["videos"] = $this->page_model->get_video_home();
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Brother - Empodera Tus Ideas', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	
				
		$this->template->write_view('content', 'layout/home/home', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}
	
	// ---	
	// Save lead into database
	// ---
	public function send_form()
	{
		
		// ---
		// verifico si registro en la base de datos o si solo loguueo
		$this->db->where("email",$_POST["email"]);
		$this->db->from('leads');
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			$session_data = array(
				'country' => $_POST["pais"],
				'name' => $_POST["nombre"],
	            'email' => $_POST["email"],
			);
		}
		else // ok register this
		{

			if(true):


			$this->db->set("country",$_POST["pais"]);
			$this->db->set("name",$_POST["nombre"]);
			$this->db->set("email",$_POST["email"]);
			$this->db->set("added_at",time());
			$this->db->set("modified_at",time());
			$this->db->insert('leads');
			$id_lead = $this->db->insert_id();

			require_once(dirname(__FILE__)."/../../libraries/GetResponseAPI.class.php");
			
			$api = new GetResponse('2avshtjsaj15bgj297bhdjp484736eo3');

			$contact_info = array(
				"email" => $_POST["email"],
				"campaign" => array(
					"campaignId" => "KdGs2",
				)
			);
			
			$api->addContact($contact_info);
			

			endif;
			
			// --
			// Saving data into session
			$session_data = array(
				'country' => $_POST["pais"],
				'name' => $_POST["nombre"],
	           	'email' => $_POST["email"],
			);	
		}

		// Add user data in session
		$this->session->set_userdata('logged_in_front', $session_data);

		echo $_POST['url'];
	}

	public function getresponse()
	{
		require_once(dirname(__FILE__)."/../../libraries/GetResponseAPI.class.php");
		
		$api = new GetResponse('2avshtjsaj15bgj297bhdjp484736eo3');

		$contact_info = array(
			"email" => 'hernanebaigorria@gmail.com',
			"campaign" => array(
				"campaignId" => "KdGs2",
			)
		);
		
		$api->addContact($contact_info);

		//print_r($api);
		exit;
	}

	public function login_form()
	{
		
		// ---
		// verifico si registro en la base de datos o si solo loguueo
		$this->db->where("email",$_POST["email"]);
		$this->db->from('leads');
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			$exist = $query->result();
			
			$session_data = array(
				'country' => $exist[0]->country,
				'name' => $exist[0]->name,
	            'email' => $exist[0]->email,
			);

			echo $_POST['url'];
		}
		else // ok register this
		{
			echo "error";	
		}

		// Add user data in session
		$this->session->set_userdata('logged_in_front', $session_data);


		
	}
	
	// salir de mi cuenta
	public function logout() {
		$this->session->unset_userdata('logged_in_front', null);
		 redirect(base_url());
	}
	
	
}
